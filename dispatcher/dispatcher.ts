/**
 * Created by alex on 21.06.17.
 */
import {ListenerListStorage} from "./emitter/listener";
import {ManagerGroup} from "./manager/group";
import {ManagerCollection} from "./manager/collection";
import {StreamDispatcher} from "./stream/dispatcher";
import {ListenerListQueue} from "./listener/list/queue";
import {ListenerListGroup} from "./listener/list/group";

export class Dispatcher {

    listeners: ListenerListStorage;

    constructor(listeners?: ListenerListStorage) {
        if (listeners !== undefined) {
            this.listeners = listeners;
        } else {
            this.listeners = new ListenerListStorage();
        }
    }

    /**
     * @description группа
     * @returns {ManagerGroup}
     */
    group(): ManagerGroup {
        return new ManagerGroup(this);
    }

    /**
     *
     * @returns {ManagerCollection}
     */
    collection(): ManagerCollection {
        return new ManagerCollection(this);
    }


    queue(): ListenerListQueue  {
        if(this.listeners.list instanceof ListenerListQueue){
            return this.listeners.list;
        }
        throw new Error('dispatcher::dispatch listeners.list');

    }

    dispatch(params?:Object): StreamDispatcher {
        let event = this.queue().start();
        let stream = new StreamDispatcher(this);
        if(typeof params === 'object'){
            for (let key in params) {
                stream.params.set(key,params[key]);
            }
        }
        stream.emitter().emit(event);
        return stream;
    }

    emit(event: string, args?: any[]): StreamDispatcher {
        let stream = new StreamDispatcher(this);
        stream.emitter().emit(event, args);
        return stream;
    }

}