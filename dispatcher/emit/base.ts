import {StreamDispatcher} from "../stream/dispatcher";
import {Dispatcher} from "../dispatcher";

export abstract class EmitBase {
    core: Dispatcher;
    stream: StreamDispatcher;

    constructor(core: Dispatcher,stream:StreamDispatcher) {
        if (core === undefined && stream === undefined) {
            throw new Error();
        }
        this.core = core;
        this.stream = stream;
    }
}
