import {EmitterCollection} from "../emitter/collection";
import {EmitterGroup} from "../emitter/group";
import {EmitBase} from "./base";
import {each, series} from "async";
import {StreamDispatcher} from "../stream/dispatcher";

export interface iEmitCallbackTask {
    (callback: iEmitCallbackJob): void;
}

// export type iEmitCallbackTask = (callback: iEmitCallbackJob)=> void|Function;

export interface iEmitCallbackJobBase<T, E> {
    (err?: E, result?: T): void;
}

export interface iEmitCallbackJob extends iEmitCallbackJobBase <any, Error> {
}

export interface iEmitCallbackEmit {
    emit(stream: StreamDispatcher, callback: iEmitCallbackJob, args?: any): void;
}

export type iEmitCallbackListener = iEmitCallbackEmit | iEmitCallbackTask;
export type iEmitCallbackListeners = iEmitCallbackListener | iEmitCallbackListener[];

export class EmitCallback extends EmitBase {

    // func(callback: iEmitCallbackJob, listener: iEmitCallbackTask, args?: any): void {
    func(callback: iEmitCallbackJob, listener: any, ...args): void {
        // console.log('callback.func', callback, listener, args, this.stream);
        // console.log('callback.func', callback, this.stream);
        if (args.length !== 0) {
            listener.call(this.stream, callback, ...args);
        } else {
            listener.call(this.stream, callback);
        }
    }

    tasks(callback: AsyncResultArrayCallback<iEmitCallbackTask, Error>, listeners: iEmitCallbackListeners[], ...args) {
        let tasks: iEmitCallbackTask[] = [];
        each(listeners, (listener: iEmitCallbackListeners, error: ErrorCallback<Error>) => {
            switch (typeof listener) {
                case 'function':

                    tasks.push((callback: iEmitCallbackJob) => {
                        this.func(callback, listener, ...args);
                    });
                    break;
                case 'object':
                    if (listener instanceof Array) {
                        tasks.push((callback: iEmitCallbackJobBase<iEmitCallbackTask[], Error>) => {
                            this.tasks(callback, listener, ...args);
                        });
                    }
                    else if (listener instanceof EmitterCollection) {
                        tasks.push((callback: iEmitCallbackJob) => {
                            this.collection(callback, listener, ...args);
                        });
                    }
                    else if (listener instanceof EmitterGroup) {
                        tasks.push((callback: iEmitCallbackJob) => {
                            this.group(callback, listener, ...args);
                        });
                    }
                    else {
                        error(new Error());
                        return;
                    }
                    break;
                /**
                 * @todo добавить стринговые события
                 *
                 case 'string':
                 tasks.push((callback) => {
                            this.emit(callback, listener, args);
                        });
                 break;
                 */
                default:
                    error(new Error());
                    return;
            }

            error();
        }, (err) => {
            callback(err, tasks);
        });
    }

    array(callback: iEmitCallbackJobBase<iEmitCallbackTask[], Error>, listeners: iEmitCallbackListeners[], ...args) {
        this.tasks(
            (err, result: iEmitCallbackTask[]) => {
                let tasks = result.filter(function (v) {
                    return (v !== undefined);
                });
                if (tasks.length === 0) {
                    callback();
                    return;
                }
                series(tasks, callback);
            },
            listeners,
            ...args);

    }

    collection(callback: iEmitCallbackJob, listener: EmitterCollection, ...args) {
        listener.emit(this.stream, callback, ...args);
    }

    group(callback: iEmitCallbackJob, listener: EmitterGroup, ...args) {
        // console.log('callback.group', arguments);
        listener.emit(this.stream, callback, ...args);
    }

    /**
     * @todo добавить event(string) в событие
     * event(callback:iEmitCallbackTask, event: string, args?: any[]) {
            if (event === undefined && !this.core.listeners.exitsEvent(event)) {
                callback();
                return;
            }
            let listener = this.core.listeners.getListener(event);
            this.emit(callback, listener, args);
        }
     */


    emit(callback: iEmitCallbackJob, listener: iEmitCallbackListeners, ...args): boolean {
        // console.log('callback.emit',callback,listener,args);
        if (listener instanceof Array) {
            // console.log('callback.emit is array');
            /**
             * @todo добавить кэширование
             */
            this.array(callback, listener, ...args);
            return true;
        }
        else if (typeof listener === 'function') {
            // console.log('callback.emit is function');
            this.func(callback, listener, ...args);
            return true;
        } else if (listener instanceof EmitterCollection) {
            // console.log('callback.emit is collection');
            this.collection(callback, listener, ...args);
            return true;
        } else if (listener instanceof EmitterGroup) {
            // console.log('callback.emit is group');
            this.group(callback, listener, ...args);
            return true;
        }
        /**
         * @todo добавить event(string) в событие
         */
        /**
         * @todo добавить dispatcher в события
         *
         else if (listener instanceof Dispatcher) {
                return listener.dispatch.apply(listener, args);
            }
         */

        return false;
    }
}