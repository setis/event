import {EmitCallback} from "./callback";
import {EmitBase} from "./base";
import {StreamDispatcher} from "../stream/dispatcher";
import {Dispatcher} from "../dispatcher";
import {iEmitCallbackJob} from "../index";

/**
 * Created by alex on 12.03.17.
 */

export class EmitNext extends EmitBase {

    callback: EmitCallback;

    constructor(core: Dispatcher, stream: StreamDispatcher) {
        super(core, stream);
        this.callback = new EmitCallback(core, stream);
    }

    next(event: string): iEmitCallbackJob {
        return () => {
            // console.log('next.next', event);
            this.emit(event);
        };

    }

    emit(event: string, ...args): boolean {
        // console.log('next.emit', event,  (this.stream.isParams(event))?this.stream.getParams(event):args, this.stream.data);
        this.stream.event = event;
        let next = this.core.queue().emit(event);
        let isEvent = this.core.listeners.exitsEvent(event);
        // console.log('next.emit',event,next,this.core.queue().list);
        /**
         * если нету текущего событие  в хранилище переходим сразу к следуещему
         */
        var i = 0;
        if (!isEvent) {
            /**
             * если нету следущего события заверашаем
             */
            if (next === false) {
                // console.log(`next.emit current:${event} next:${next} i:${++i} 42`);
                return false;
            }
            // console.log(`next.emit current:${event} next:${next} i:${++i} 45`);
            return this.emit(next);
        } else {
            i = 2;
            let buffer;
            if (this.stream.params.has(event) && (buffer = this.stream.params.get(event)) !== undefined) {
                args = buffer;
            }
            /**
             * если нету следущего события выполняем
             */
            if (next === false) {
                return this.callback.emit(() => {
                    // console.log(`next.emit current:${event} next:${next} i:${++i} 53`);
                }, this.core.listeners.getListener(event), ...args);
            }
            /**
             * если есть текущего событие в хранилище
             */
            if (this.core.listeners.exitsEvent(next)) {
                // console.log(`next.emit current:${event} next:${next} i:${++i} 61`);
                return this.callback.emit(this.next(next), this.core.listeners.getListener(event), ...args);
            }
            /**
             * если нету текущего событие в хранилище ищем которые есть следующиеся
             */
            // var eventWhile = next;
            // while (eventWhile === false || this.core.listeners.exitsEvent(eventWhile) === true) {
            //     eventWhile = this.core.queue().emit(eventWhile);
            //     // console.log(this.core.listeners.exitsEvent(eventWhile),eventWhile)
            // }
            // if (typeof eventWhile === 'boolean') {
            //     console.log(`next.emit current:${event} next:${next} i:${++i} 72`, eventWhile);
            //     return false;
            // }
            // console.log(`next.emit current:${event} next:${next} i:${++i} 76`, eventWhile);
            // return this.emit(eventWhile);
            return this.callback.emit(this.next(next), this.core.listeners.getListener(event), ...args);
        }
    }
}