import {EmitterBase} from "./base";
import {map, series} from "async";
import {ListenerListStorage} from "./listener";
import {ListenerStorageHasOne} from "../listener/storage/has_one";
import {ListenerListQueue} from "../listener/list/queue";
import {StreamDispatcher} from "../stream/dispatcher";
import {iEmitCallbackJob, iEmitCallbackTask} from "../emit/callback";


export class EmitterAction extends EmitterBase {

    listeners: ListenerListStorage;

    constructor(listeners?: ListenerListStorage) {
        super();
        if (listeners !== undefined && listeners instanceof ListenerListStorage) {
            this.listeners = listeners;
        } else {
            this.listeners = new ListenerListStorage(new ListenerStorageHasOne(), new ListenerListQueue());
        }
    }

    emit(stream: StreamDispatcher, callback: iEmitCallbackJob, args?: any) {
        let events = this.listeners.events();
        if (events.length === 0) {
            // console.log('not found events', this.listeners.events);
            callback();
            return;
        }
        var emit = stream.emitter().callback;
        map(events, (event: string, callback:(error?:Error,result?:iEmitCallbackTask|iEmitCallbackTask[])=>void) => {
            console.log('emitter.group', event, this.listeners.exitsEvent(event));
            if (!this.listeners.exitsEvent(event)) {
                callback();
                return;
            }
            if (this.listeners.hasListener(event) === false) {
                callback();
                return;
            }
            let listeners = this.listeners.getListener(event);

            // console.log('group.emit0', listeners);
            if (listeners instanceof Array) {
                emit.tasks(function (err, tasks:iEmitCallbackTask[]) {
                    console.log('group.emit1', tasks);
                    callback(err, tasks);
                }, listeners, stream.params.has(event) ? stream.params.get(event) : args);
            } else {
                // console.log('emitCollection:listener',listeners,event);
                callback(undefined, function (callback) {
                    // console.log('group.emit2', listeners);
                    emit.emit(callback, listeners, stream.params.has(event) ? stream.params.get(event) : args);
                });
            }

        }, function (err, result:iEmitCallbackTask[]) {
            // console.log('group.emit', arguments);
            if (err) {
                throw err;
            }
            if (result instanceof Array && result.length !== 0) {
                let tasks = result.filter((item) => {
                    return item !== null;
                });
                if (tasks.length === 0) {
                    callback();
                    return;
                }
                series(tasks, callback);
            }else{
                callback();
            }

        });
    }

}