import {ListenerListStorage} from "./listener";
import {ListenerListGroup} from "../listener/list/group";
import {ListenerStorageBase} from "../listener/storage/base";
import {iEmitCallbackEmit, iEmitCallbackJob} from "../emit/callback";
import {StreamDispatcher} from "../stream/dispatcher";
/**
 * Created by alex on 11.03.17.
 */

export abstract class EmitterBase implements iEmitCallbackEmit {


    listeners: ListenerStorageBase | ListenerListStorage;
    protected _parent: any;
    protected _event: string;

    // constructor(event: string, parent: any) {
    //     this.event = event;
    //     this.parent = parent;
    // }

    get parent(): any {
        return this._parent;
    }

    set parent(value: any) {
        this._parent = value;
    }

    isParent(): boolean {
        return this._parent !== undefined;
    }

    get event(): string {
        if (this.listeners instanceof ListenerListStorage && this.listeners.list instanceof ListenerListGroup) {
            return this.listeners.list.event;
        }
        return this._event;
    }

    set event(value: string) {
        if (this.listeners instanceof ListenerListStorage && this.listeners.list instanceof ListenerListGroup) {
            this.listeners.list.event = value;
            return;
        }
        this._event = value;
    }

    events(): string[] {
        if (this.listeners instanceof ListenerListStorage && this.listeners.list instanceof ListenerListGroup) {
            return this.listeners.list.events();
        }
        if (this.listeners instanceof ListenerStorageBase) {
            return this.listeners.events();
        }
        return Object.keys(this.listeners);
    }


    abstract  emit(stream: StreamDispatcher, callback: iEmitCallbackJob, args?: any): void;

}