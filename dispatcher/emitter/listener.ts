import {ListenerListQueue} from "../listener/list/queue";
import {ListenerStorageHasOne} from "../listener/storage/has_one";
import {ListenerListGroup} from "../listener/list/group";
import {ListenerStorageDouble} from "../listener/storage/double";
import {EmitterGroup} from "./group";
import {EmitterCollection} from "./collection";
import {iEmitCallbackListeners} from "../emit/callback";
/**
 * Created by alex on 11.03.17.
 */

export class ListenerListStorage {
    list: ListenerListQueue | ListenerListGroup;
    storage: ListenerStorageHasOne | ListenerStorageDouble;

    constructor(storage?: ListenerStorageHasOne | ListenerStorageDouble, list?: ListenerListQueue | ListenerListGroup) {
        if (list !== undefined) {
            this.list = list;
        } else {
            this.list = new ListenerListQueue();
        }
        if (storage !== undefined) {
            this.storage = storage;
        } else {
            this.storage = new ListenerStorageHasOne();
        }
    }

    on(event: string, listener: Function | EmitterGroup | EmitterCollection): this {
        this.storage.on.apply(this.storage, arguments);
        return this;
    }

    off(event: string, listener?: Function) {
        this.storage.off.apply(this.storage, arguments);
        return this;
    }

    events(): string[] {
        return this.list.events();
    }

    exitsEvent(event: string): boolean {
        return (this.list.exist(event) && this.storage.exitsEvent(event));
    }

    getListener(event: string):iEmitCallbackListeners  {
        return this.storage.getListener(event);
    }
    hasListener(event: string):boolean  {
        return this.storage.hasListener(event);
    }

    emit(event: string):iEmitCallbackListeners | false {
        let name;
        if (this instanceof ListenerListQueue) {
            name = this.list.emit(event);
            if (!name) {
                return false;
            }
        } else {
            name = event;
        }
        if (!this.list.exist(name) || !this.storage.exitsEvent(name)) {
            return false;
        }
        return this.storage.getListener(name);
    }
}