/**
 * Created by alex on 11.03.17.
 */

export class ListenerListBase {
    list: string[];
    uniq:boolean;

    constructor(list?: string[]) {
        if (list !== undefined) {
            this.list = list;
        } else {
            this.list = [];
        }
        this.uniq = true;
    }

    events(): string[] {
        return this.list;
    }

    exist(event: string):boolean {
        return (this.list.indexOf(event) !== -1);
    }

    emit(event: string): false|string {
        if (this.list[event] === undefined) {
            return false;
        }
        return this.list[event];
    }
    push(event:string){
        if(this.uniq && this.list.indexOf(event) !== -1){
            throw new Error(`already ${event}`);
        }
        this.list.push(event);
    }
    delete(event:string){
        let index = this.list.indexOf(event);
        if(index !==-1){
            this.list.splice(index,1);
        }
    }
    after(search:string,event:string|string[]){
        if(event instanceof Array){
            let index = this.list.indexOf(search);
            if (index === -1) {
                throw new Error(` Queue not found event:${search}`);
            }
            if(this.uniq){
                for (let name of event) {
                    this.list.indexOf('')
                }
            }
            this.list = [...this.list.slice(0, index),...event,...this.list.slice(index + 1)];
        }else{
            let index = this.list.indexOf(search);
            if (index === -1) {
                throw new Error(` Queue not found event:${search}`);
            }
            this.list = [...this.list.slice(0, index),event,...this.list.slice(index + 1)];
        }

    }
    add(...args): this {
        var index: number, id: number;
        switch (args.length) {
            case 1:
                if (args[0] instanceof Array) {
                    args[0].forEach((event) => {
                        id = this.list.push(event);
                        this.reset(this.list[id - 2]);
                    });
                    // console.log(`queque.add array`, args[0], this.list);
                } else {
                    id = this.list.push(args[0]);
                    this.reset(this.list[id - 2]);
                    // console.log(`queque.add string`, args[0], this.list);
                }

                break;
            case 2:
                index = this.list.indexOf(args[0]);
                // console.log(this.list,args);
                if (index === -1) {
                    throw new Error(` Queue not found event:${args[0]}`);
                }
                let buffer:string[] = this.list.slice(0, index);
                buffer.push(args[1]);
                this.list = buffer.concat(this.list.slice(index + 1));
                this.reset(this.list.slice(index + 1));
                break;
            case 3:
                index = this.list.indexOf(args[0]);
                // console.log(this.list,args);
                if (index === -1) {
                    throw new Error(` Queue not found event:${args[0]}`);
                }
                if (args[2]) {
                    // console.log(index - 1 < 0)
                    //before
                    if (index - 1 < 0) {
                        if (args[1] instanceof Array) {
                            args[1].forEach((event) => {
                                this.list.unshift(event);
                            });
                        } else {
                            this.list.unshift(args[1]);
                        }
                    } else {
                        // console.log(this.list.slice(0, index), index, args[1], this.list.slice(index), index );
                        let buffer:string[] = this.list.slice(0, index);
                        buffer.push(args[1]);
                        this.list = buffer.concat(this.list.slice(index));
                    }
                } else {

                    //after
                    if (index - 1 < 0) {
                        if (args[1] instanceof Array) {
                            args[1].forEach((event) => {
                                this.list.push(event);
                            });
                        } else {
                            this.list.push(args[1]);
                        }
                    } else {
                        // console.log(this.list.slice(0, index+1), index+1, args[1], this.list.slice(index+1), index+1 );
                        let buffer:string[] = this.list.slice(0, index+1);
                        buffer.push(args[1]);
                        this.list = buffer.concat(this.list.slice(index+1));
                    }
                }

                break;
        }
        // console.log(this.list,args);
        return this;
    }

    remove(...args): this {
        let index: number;
        switch (args.length) {
            case 1:
                if(this.list.length === 0){
                    return this;
                }
                index = this.list.indexOf(args[0]);
                if (index === -1) {
                    console.log(` Queue not found event:${args[0]}`);
                    break;
                }
                this.list.splice(index, 1);
                break;
            case 2:
                if(this.list.length === 0){
                    return this;
                }
                index = this.list.indexOf(args[0]);
                if (index === -1) {
                    console.log(` Queue not found event:${args[0]}`);
                    break;
                }
                let buffer:string[] = this.list.slice(0, index);
                buffer.push(args[1]);
                this.list = buffer.concat(this.list.slice(index+1));
                break;
        }
        return this;
    }
    reset(event: string|string[]) {
    };

}