import {ListenerListBase} from "./base";
/**
 * Created by alex on 10.03.17.
 */

export class ListenerListGroup extends ListenerListBase {
    name: string;

    constructor(name?: string, list?: string[]) {
        super(list);
        if(name !== undefined){
            this.name = name;
        }
    }

    get event(): string {
        return this.name;
    }

    set event(name: string) {
        this.name = name;
    }


}