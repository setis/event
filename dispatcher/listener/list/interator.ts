/**
 * Created by alex on 10.03.17.
 */

export class ListenerListInterator {
    from: number;
    to: number;
    current: number;
    private _list: string[];

    constructor(list?: string[]) {
        if (list !== undefined) {
            this.list = list;
        }
    }

    get list(): string[] {
        return this._list;
    }

    set list(value: string[]) {
        this._list = value.map((v) => {
            return v;
        });
        this.from = 0;
        this.to = this._list.length - 1;
    }

    next() {
        if (this.current === undefined) {
            // инициализация состояния итерации
            this.current = this.from;
        }

        if (this.current <= this.to) {
            return {
                done: false,
                value: this._list[this.current++]
            };
        } else {
            // очистка текущей итерации
            delete this.current;
            return {
                value:undefined,
                done: true
            };
        }
    }
}