import {ListenerListBase} from "./base";
import {ListenerListInterator} from "./interator";
/**
 * Created by alex on 16.02.17.
 */

export class ListenerListQueue extends ListenerListBase {

    interator():ListenerListInterator {
        return new ListenerListInterator(this.list);
    }

    /**
     *
     * @param event
     * @returns {string}
     * @returns {boolean} false
     */
    emit(event: string):any {
        return this.next(event);
    }

    start(): string {
        return this.list[0];
    }

    end(): string {
        return this.list[this.list.length - 1];
    }

    next(event: string): any {
        let index = this.list.indexOf(event);
        if (index === -1 || this.list[index + 1] === undefined) {
            return false;
        }
        return this.list[index + 1];
    }


    prev(event: string): string|false {
        let index = this.list.indexOf(event);
        // console.log(event, index, this.list);
        if (index === -1 || this.list[index - 1] === undefined) {
            return false;
        }
        return this.list[index - 1];
    }



    position(event: string): number|false {
        let index = this.list.indexOf(event);
        if (index === -1 || this.list[index] === undefined) {
            return false;
        }
        return index;
    }

    current(position: number): string| false {
        if (this.list[position] === undefined) {
            return false;
        }
        return this.list[position];
    }


}