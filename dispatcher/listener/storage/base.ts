import {ListenerStorageHasOne} from "./has_one";
import {iEmitCallbackListener, iEmitCallbackListeners} from "../../emit/callback";
/**
 * Created by alex on 10.03.17.
 */

export abstract class ListenerStorageBase {
    listeners: any = {};

    abstract on(event: string, listener: iEmitCallbackListener): this;

    abstract off(event: string, listener?: iEmitCallbackListener): this;

    abstract once(event: string, listener: iEmitCallbackListener): this;

    abstract emit(event: string): false | iEmitCallbackListeners;

    exitsEvent(event: string): boolean {
        if (this.listener_many()) {
            return (this.listeners[event] !== undefined && (this.listeners[event] instanceof Array && this.listeners[event].length));
        }
        return (this.listeners[event] !== undefined);
    }

    events(): string[] {
        return Object.keys(this.listeners);
    }

    abstract getListener(event: string): iEmitCallbackListeners | false;

    listener_many(): boolean {
        return (this instanceof ListenerStorageHasOne) ? false : true;
    }
}