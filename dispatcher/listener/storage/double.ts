import {ListenerStorageHasOne} from "./has_one";
import {ListenerStorageBase} from "./base";
import {uniq} from "lodash";
import {iEmitCallbackListener, iEmitCallbackListeners} from "../../emit/callback";
/**
 * Created by alex on 14.03.17.
 */

export class ListenerStorageDouble extends ListenerStorageBase {

    read: ListenerStorageHasOne|ListenerStorageDouble;
    write: ListenerStorageHasOne|ListenerStorageDouble;

    constructor(listener?: ListenerStorageHasOne|ListenerStorageDouble) {
        super();
        if (listener !== undefined) {
            this.read = listener;
        }
        this.write = new ListenerStorageHasOne();
    }

    setReadListener(listener: ListenerStorageHasOne|ListenerStorageDouble): this {
        this.read = listener;
        return this;
    }

    setWriteListener(listener: ListenerStorageHasOne|ListenerStorageDouble): this {
        this.write = listener;
        return this;
    }

    events(): string[] {
        let events: string[] = this.write.events();
        events.concat(this.read.events());
        return uniq(events);
    }

    exitsEvent(event: string): boolean {
        return (!this.write.exitsEvent(event) || !this.read.exitsEvent(event));
    }

    on(event: string, listener: iEmitCallbackListener): this {
        this.write.on(event, listener);
        return this;
    }

    off(event: string, listener?:  iEmitCallbackListener): this {
        this.write.off(event, listener);
        return this;
    }

    once(event: string, listener: iEmitCallbackListener): this {
        this.write.once(event, listener);
        return this;
    }

    getListener(event: string):iEmitCallbackListeners{
        if (event === 'parse.cheerio.content') {
            // console.log(this.write.listeners,this.read.listeners);
            console.log('double', event, this.write.exitsEvent(event), this.write[event], this.read.exitsEvent(event), this.read[event]);
        }

        if (this.write.exitsEvent(event) !== false) {
            return this.write.getListener(event);
        }
        if (this.read.exitsEvent(event) !== false) {
            return this.read.getListener(event);
        }
        throw new Error('not found listener')
    }
    hasListener(event: string): boolean {
        return (this.write.exitsEvent(event) || this.read.exitsEvent(event));
    }

    emit(event: string) {
        let func;
        func = this.write.emit(event);
        if (func) {
            return func;
        }
        func = this.read.emit(event);
        return func;
    }


}