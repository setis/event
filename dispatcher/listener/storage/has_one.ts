import {ListenerStorageBase} from "./base";
import {iEmitCallbackListener, iEmitCallbackListeners} from "../../emit/callback";
/**
 * Created by alex on 10.03.17.
 *
 Связь один-к-одному — когда одной записи в таблице соответствует только одна запись в другой таблице. Допустим, если у нас есть таблица с фамилиями студентов и таблица с номерами зачетных книжек, то связь между ними будет один-к-одному, так как у одного человека может быть только одна зачетка (в пределах ВУЗ-а конечно). Впрочем, у меня было две зачетки, когда я, уже занимаясь написанием диплома по одной специальности, пошел на другую, но мы этот момент здесь не учитываем.

 Связь один-ко-многим — когда одной записи в таблице соответствует несколько записей в другой таблице. Приведу уже набивший оскомину пример, как книги и авторы. Мы не рассматриваем книги, написанные несколькими авторами, поэтому в данном случае одному автору может соответствовать несколько книг, но книге — только один автор.

 Связь много-к-одному — все аналогично рассмотренной выше записи один-ко-многим. Много книг, но у них один автор.

 Связь много-ко-многим — когда нескольким записям в таблице соответствует несколько записей в другой таблице. Наглядный и часто используемый на сайтах пример — фильмы и жанры. Одному жанру может соответствовать несколько фильмов (фантастика — Прометей, Живая сталь, Вспомнить все итд). Но фильмы также могут быть нескольких жанров: (8 первых свиданий — это и комедия, и мелодрама одновременно).


 */
export class ListenerStorageHasOne extends ListenerStorageBase {

    once(event: string, listener: iEmitCallbackListener): this {
        throw new Error("Method not implemented.");
    }


    replace: boolean = false;

    on(event: string, listener: iEmitCallbackListener): this {
        if (this.listeners[event] === undefined) {
            this.listeners[event] = listener;
        } else {
            if (this.listeners[event] !== listener) {
                if (this.replace) {
                    this.listeners[event] = listener;
                } else {
                    throw new Error(`listener.storage.has_one event:${event} already exists listener`);
                }
            } else {
                if (this.replace) {
                    this.listeners[event] = listener;
                } else {
                    throw new Error(`listener.storage.has_one event:${event} already exists does not match listener`);
                }
            }
        }
        return this;
    }

    off(event: string, listener?: iEmitCallbackListener): this {
        if (this.listeners[event] !== undefined && (listener !== undefined && this.listeners[event] === listener)) {
            delete this.listeners[event];
        }
        return this;
    }


    emit(event: string): false | iEmitCallbackListeners {
        let listener = this.listeners[event];
        return (listener === undefined) ? false : listener;
    }

    getListener(event: string): iEmitCallbackListeners {
        return this.listeners[event];
    }

    hasListener(event: string): boolean {
        return (this.listeners[event] !== undefined);
    }

}