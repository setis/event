import {Dispatcher} from "../dispatcher";
/**
 * Created by alex on 11.03.17.
 */
export class ManagerBase {
    content: Dispatcher;

    constructor(content: Dispatcher) {
        if (content instanceof Dispatcher) {
            this.content = content;
        }
    }

}