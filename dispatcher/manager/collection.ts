import {EmitterCollection} from "../emitter/collection";
import {Dispatcher} from "../dispatcher";
/**
 * Created by alex on 11.03.17.
 */

export class ManagerCollection  {

    content: Dispatcher;

    constructor(content: Dispatcher) {
        if (content instanceof Dispatcher) {
            this.content = content;
        }
    }
    create(event: string, events?: string[]): EmitterCollection {
        var obj = new EmitterCollection();
        obj.listeners.storage = this.content.listeners.storage;
        obj.event = event;
        obj.parent = this.content;
        if (events !== undefined) {
            obj.listeners.list.add(events);
        }
        this.content.listeners.on(event, obj);
        return obj;
    }
}