import {EmitterGroup} from "../emitter/group";
import {ListenerListStorage} from "../emitter/listener";
import {ListenerListQueue} from "../listener/list/queue";
import {ListenerStorageDouble} from "../listener/storage/double";
import {Dispatcher} from "../dispatcher";
/**
 * Created by alex on 11.03.17.
 */

export class ManagerGroup {
    content: Dispatcher;

    constructor(content: Dispatcher) {
        if (content instanceof Dispatcher) {
            this.content = content;
        }
    }
    create(event: string, events?: string[]): EmitterGroup {
        let obj = new EmitterGroup(new ListenerListStorage(this.content.listeners.storage, new ListenerListQueue()));
        // let obj = new EmitterGroup(this.content.listeners.storage);
        // obj.listeners.storage = this.content.listeners.storage;
        obj.event = event;
        obj.parent = this.content;
        if (events !== undefined) {
            obj.listeners.list.add(events);
        }
        this.content.listeners.on(event, obj);
        return obj;
    }

    get(event: string): EmitterGroup | false {
        var storage = this.content.listeners.storage;
        var listener = storage.getListener(event);
        if (!listener) {
            return false;
        }
        if (listener instanceof Array) {
            throw new Error();
        } else if (listener instanceof EmitterGroup) {
            return listener;
        }
        return false;

    }

    createDouble(event: string, events?: string[]): EmitterGroup {
        let obj = new EmitterGroup(new ListenerListStorage(new ListenerStorageDouble(this.content.listeners.storage), new ListenerListQueue()));
        // var obj = new EmitterGroup();
        // obj.listeners.storage = new ListenerStorageDouble(this.content.listeners.storage);
        obj.event = event;
        obj.parent = this.content;
        if (events !== undefined) {
            obj.listeners.list.add(events);
        }
        this.content.listeners.on(event, obj);
        return obj;
    }
}