/**
 * Created by alex on 11.03.17.
 */
import uuidV4 = require('uuid/v4');
import {StreamEntry} from "./entry";
export class StreamBase {
    uuid: string;
    time: Date;
    event: string;
    data: Map<string,any>;
    params: Map<string,any>;
    entry: StreamEntry;
    propagationStopped: boolean;

    constructor() {
        this.uuid = uuidV4();
        this.time = new Date();
        this.data = new Map();
        this.params = new Map();
        this.entry = new StreamEntry();
        this.propagationStopped = false;
    }

    // setParams(event: string, args): this {
    //     this.params[event] = args;
    //     return this;
    // }
    //
    // getParams(event: string): any {
    //     return (this.params[event] === undefined) ? [] : this.params[event];
    // }
    //
    // isParams(event: string): boolean {
    //     return (this.params[event] !== undefined);
    // }
    //
    // setData(key: string, value: any): this {
    //     this.data[key] = value;
    //     return this;
    // }
    //
    // getData(key: string): any {
    //     return clone(this.data[key]);
    // }

    isPropagationStopped() {
        return this.propagationStopped;
    }

    stopPropagation() {
        return this.propagationStopped = true;
    }
}