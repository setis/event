/**
 * Created by alex on 17.02.17.
 */

import uuidV4 = require('uuid/v4');
import {StreamBase} from "./base";
import {Dispatcher} from "../dispatcher";
import {EmitNext} from "../emit/next";
import {ManagerGroup} from "../manager/group";
import {ManagerCollection} from "../manager/collection";

export class StreamDispatcher extends StreamBase {
    core: Dispatcher;
    event: string;

    constructor(kernel: Dispatcher) {
        super();
        this.core = kernel;

    }

    dispatch() {
        let event = this.core.queue().start();
        let args;
        if (this.params.has(event) && ((args = this.params.get(event)) !== undefined)) {
            return this.emitter().emit(event, ...args);
        }
        return this.emitter().emit(event);
    }

    private _emitter: EmitNext;

    emitter(): EmitNext {
        if (this._emitter === undefined) {
            this._emitter = new EmitNext(this.core, this);
        }
        return this._emitter;
    }

    group(): ManagerGroup {
        return this.core.group();
    }

    collection(): ManagerCollection {
        return this.core.collection();
    }


    next() {
    }

    stop() {
    }


}