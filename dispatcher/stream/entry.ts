import {EmitterCollection} from "../emitter/collection";
import {EmitterGroup} from "../emitter/group";
export interface EntryParams{
    group:boolean;
    parallel:boolean;
}
export class StreamEntry {

    event: string;
    parent: StreamEntry;
    childern: Set<StreamEntry>;
    listeners: any;
    params: EntryParams;
    config ={
        listener:false
    };
    events:string[] = [];
    point:number[];
    constructor(parent?: StreamEntry) {
        if (parent !== undefined) {
            this.parent = parent;
        }
    }
    static detect(listener):EntryParams {
        if (typeof listener === 'function') {
            return {
                group: false,
                parallel: false,
            };
        } else if (listener instanceof EmitterCollection) {
            return {
                group: true,
                parallel: true,
            };
        } else if (listener instanceof EmitterGroup) {
            return {
                group: true,
                parallel: false,
            };
        }
        throw new Error();
    }

    isGroup():boolean{
        return this.params.group;
    }
    isParallel():boolean{
        return this.params.parallel;
    }
    attach(event: string, listener): this|StreamEntry {
        this.event = event;
        this.events.push(event);
        if(this.config.listener){
            this.listeners = listener;
        }
        this.params = StreamEntry.detect(listener);

        return this;
    }

    detach(): this {
        if(this.childern !==undefined){
            this.childern.clear();
        }
        if (this.parent.childern !== undefined) {
            this.parent.childern.delete(this);
        }
        const list = ['event','parent','listeners','params'];
        list.forEach((name)=>{
            this[name] = null;
        });
        return this;
    }


    entries():any{
        return this.events;
    }
    layer():number{
        return 0;
    }
    child(): StreamEntry {
        if (this.childern === undefined) {
            this.childern = new Set();
        }
        let child = new StreamEntry(this);
        this.childern.add(child);
        return child;
    }
    sib(){

    }


}