import {StreamDispatcher} from "../stream/dispatcher";
import {EmitterGroup} from "../emitter/group";
import {EmitterCollection} from "../emitter/collection";
import {EmitNext} from "../emit/next";
/**
 * Created by alex on 18.03.17.
 */

export class Manager {



    stream: StreamDispatcher;
    container;
    time: Date;

    constructor(stream: StreamDispatcher) {
        this.stream = stream;
    }
    get event():string{
        return '';
    };
    // get data(): any {
    //     return this.stream.data;
    // }
    //
    // set data(value: any) {
    //     this.stream.data = value;
    // }
    // get params(): any {
    //     return this.stream.params;
    // }
    //
    // set params(value: any) {
    //     this.stream.params = value;
    // }
    get uuid(): string {
        return this.stream.uuid;
    }
    //
    // setParams(event: string, ...args): this {
    //     this.stream.setParams.apply(this.stream, arguments);
    //     return this;
    // }
    //
    // getParams(event: string): any {
    //     return this.stream.getParams(event);
    // }
    //
    // isParams(event: string): boolean {
    //     return this.stream.isParams(event);
    // }
    //
    // setData(key: string, value: any): this {
    //     this.stream.setData(key, value);
    //     return this;
    // }
    //
    // getData(key: string): any {
    //     return this.stream.getData(key);
    // }

    emitter():EmitNext{
        return this.stream.emitter();
    }

    dispach(){
        this.stream.dispatch()
    }
    attachContainer(){}
    detachContainer(){

    }


    isGroup(): boolean {
        return (this.container instanceof EmitterGroup || this.container instanceof EmitterCollection);

    }

    isAsync(): boolean {
        return  (this.container instanceof EmitterCollection);
    }

    isRouter(): boolean {
        return false;
    }
}