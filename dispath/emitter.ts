import {EventDipatcherQueueUniq} from "./queue/uniq";
import {EventDispatcherStream, EventDispatcherStreamConfig} from "./stream";
import {EventDispatcherListenerFace} from "./listener/index";
import {EventDispatcherListenerOnce} from "./listener/once";
import {EventDispatcherDoubleMode} from "./listener/double/index";

export interface EventDispatcherBind {
    (stream: EventDispatcherStream): Promise<any>;
}

export class EventDispatcherEmitter implements EventDispatcherListenerFace {


    listeners: EventDispatcherListenerFace;
    queue: EventDipatcherQueueUniq;
    onCache: boolean;
    cache: EventDispatcherBind[];
    config: EventDispatcherStreamConfig;

    constructor(listener: EventDispatcherListenerFace = new EventDispatcherListenerOnce(), config: EventDispatcherStreamConfig = {
        multi: false,
        mode: EventDispatcherDoubleMode.none
    }) {
        this.config = config;
        this.listeners = listener;
        this.queue = new EventDipatcherQueueUniq();
        this.onCache = true;
        this.cache = [];
    }

    static build(queue: EventDipatcherQueueUniq, listeners: EventDispatcherListenerFace): EventDispatcherBind[] {
        let arr: EventDispatcherBind[] = [];

        function link(arr: EventDispatcherBind[], event: string, func) {
            arr.push(function (stream: EventDispatcherStream): Promise<void> {
                stream.event = event;
                let buf;
                if (stream.params.has(event) && (buf = stream.params.get(event)) !== undefined) {
                    if(Array.isArray(buf)){
                        return func.apply(stream, buf);
                    }
                    return func.call(stream, buf);
                }
                return func.call(stream);
            });
        }

        /**
         * @todo добавить group,collection
         * @returns {string}
         */
        // for (let [event,async,list] of queue) {
        for (let event of queue) {
            // console.log(event);
            if (listeners.has(event)) {
                let storage = listeners.get(event);
                if (typeof storage === "function") {
                    link(arr, event, storage);
                } else {
                    if (storage instanceof Array) {
                        storage.forEach((func) => {
                            link(arr, event, func);
                        });
                    }
                    else if (storage instanceof Set) {
                        storage.forEach((func) => {
                            link(arr, event, func);
                        });
                    }
                    else {
                        throw new Error();
                    }
                }
            }
        }

        return arr;
    }

    make(listeners: EventDispatcherListenerFace, key: EventDispatcherDoubleMode = EventDispatcherDoubleMode.none) {
        if (key === EventDispatcherDoubleMode.none && this.onCache && this.cache === undefined) {
            return this.cache;
        }
        let arr = EventDispatcherEmitter.build(this.queue, listeners);
        if (key === EventDispatcherDoubleMode.none && this.onCache) {
            this.cache = arr;
        }
        return arr;
    }

    static executor(binds: EventDispatcherBind[], stream: EventDispatcherStream): Promise<EventDispatcherStream> {
        let p = Promise.resolve();
        binds.forEach(func => {
            p = p.then( () => {
                return func(stream);
            });
        });
        return p.then(() => {
            return stream;
        });
    }

    // static executor(binds: EventDispatcherBind[], stream: EventDispatcherStream): Promise<EventDispatcherStream> {
    //     let promise = Promise.resolve();
    //     binds
    //         .forEach(value => {
    //             promise.then((result) => {
    //                 console.log(result,stream)
    //                 return value(stream);
    //             });
    //         });
    //
    //     return promise.then(() => {
    //         return Promise.resolve(stream);
    //     });
    // }

    stream() {
        return new EventDispatcherStream(this.config, this);
    }

    dispatch(params?: Object): Promise<EventDispatcherStream> {
        return this.stream().dispatch(params);
    }

    list(): string[] {
        return this.listeners.list();
    }

    on(event: string, listener: any): this {
        this.listeners.on(event, listener);
        return this;
    }

    off(event: any, listener?: any): this {
        this.listeners.off(event, listener);
        return this;
    }

    get(event: string) {
        return this.listeners.get(event);
    }

    has(event: string): boolean {
        return this.listeners.has(event);
    }
}

