import {EventDispatcherListenerOnce} from "../once";
import {uniq} from "lodash";
import {EventDispatcherListenerFace} from "../index";
/**
 * Created by alex on 13.07.17.
 */

export abstract class EventDispatcherListenerDoubleBase implements EventDispatcherListenerFace{
    read: EventDispatcherListenerFace;
    write: EventDispatcherListenerFace;

    constructor(read?: EventDispatcherListenerFace, write?: EventDispatcherListenerFace) {
        if (read !== undefined) {
            this.read = read;
        }
        if (write !== undefined) {
            this.write = write;
        } else {
            this.write = new EventDispatcherListenerOnce();
        }

    }

    setReadListener(listener: EventDispatcherListenerFace) {
        this.read = listener;
    }

    setWriteListener(listener: EventDispatcherListenerFace) {
        this.write = listener;
    }

    list(): string[] {
        let events: string[] = this.write.list();
        events.concat(this.read.list());
        return uniq(events);
    }

    on(event: string, listener: Function): this {
        this.write.on(event, listener);
        return this;
    }

    off(event: string, listener?: Function): this {
        this.write.off(event, listener);
        return this;
    }

    abstract get(event: string);

    has(event: string): boolean {
        return (this.write.has(event) || this.read.has(event));
    }

}