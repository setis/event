/**
 * Created by alex on 13.07.17.
 */


import {EventDispatcherListenerDoubleBase} from "./base";
import {EventDispatcherListenerFace} from "../index";

export class EventDispatcherListenerDoubleDuplex extends EventDispatcherListenerDoubleBase implements EventDispatcherListenerFace {

    get(event: string): Set<Function> {
        let funcs = new Set();
        if (this.read.has(event)) {
            let funcRead = this.read.get(event);
            if (funcRead instanceof Set) {
                funcRead.forEach((listener) => {
                    funcs.add(listener);
                })
            } else {
                funcs.add(funcRead);
            }
        }
        if (this.write.has(event)) {
            let funcWrite = this.write.get(event);
            if (funcWrite instanceof Set) {
                funcWrite.forEach((listener) => {
                    funcs.add(listener);
                })
            } else {
                funcs.add(funcWrite);
            }
        }

        return funcs;
    }

}
