/**
 * Created by alex on 13.07.17.
 */

export * from './base';
export * from './duplex';
export * from './simplex';
export * from './fullduplex';

export enum EventDispatcherDoubleMode {
    none,
    simplex,
    duplex,
    fullduplex
}

