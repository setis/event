/**
 * Created by alex on 13.07.17.
 */


import {EventDispatcherListenerDoubleBase} from "./base";
import {EventDispatcherListenerFace} from "../index";
/**
 * Created by alex on 14.03.17.
 */

export class EventDispatcherListenerDoubleSimplex extends EventDispatcherListenerDoubleBase implements EventDispatcherListenerFace {

    get(event: string): Function | Set<Function> | void {
        if (this.write.has(event)) {
            return this.write.get(event);
        }
        if (this.read.has(event) !== false) {
            return this.read.get(event);
        }
    }



}
