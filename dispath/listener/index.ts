/**
 * Created by alex on 11.07.17.
 */
export interface EventDispatcherListenerFace {
    list(): string[];

    on(event: string, listener: any): this ;

    off(event: string, listener?: any): this;

    get(event: string): any;


    has(event: string): boolean;
}

export * from './double/index';
export * from './many';
export * from './once';