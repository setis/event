/**
 * Created by alex on 11.07.17.
 */
import {EventDispatcherListenerFace} from "./index";

export class EventDispatcherListenerMany  implements EventDispatcherListenerFace{
    listeners: Map<string, Set<Function>>;

    constructor() {
        this.listeners = new Map();
    }

    on(event: string, listener: Function): this {
        if (this.listeners.has(event)) {
            let l = this.listeners.get(event);
            if (l === undefined) {
                this.listeners.set(event, new Set([listener]));
            }
            else if (l instanceof Set) {
                l.add(listener);
            } else {
                this.listeners.set(event, new Set([l, listener]));
            }
        } else {
            this.listeners.set(event, new Set([listener]));
        }
        return this;
    }

    off(event: string, listener?: Function): this {
        if (this.listeners.has(event)) {
            if (listener === undefined) {
                this.listeners.delete(event);
            } else {
                let listeners = this.listeners.get(event);
                if (listeners instanceof Set) {
                    listeners.delete(listener);
                    if (listeners.size === 0) {
                        this.listeners.delete(event);
                    }
                }
            }
        }
        return this;
    }


    has(event: string): boolean {
        return this.listeners.has(event);
    }

    get(event: string): Set<Function> | void {
        return this.listeners.get(event);
    }

    list(): string[] {
        return Array.from(this.listeners.keys());
    }

}