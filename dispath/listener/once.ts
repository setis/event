import {EventDispatcherListenerFace} from "./index";

export class EventDispatcherListenerOnce implements EventDispatcherListenerFace {

    listeners: Map<string, Function>;

    constructor() {
        this.listeners = new Map();
    }

    list(): string[] {
        return Array.from(this.listeners.keys());
    }

    on(event: string, listener: Function): this {
        if (!this.listeners.has(event)) {
            this.listeners.set(event, listener);
            return this;
        }
        throw new Error(`listener.storage.has_one event:${event} already exists listener`);
    }

    off(event: string): this {
        if (this.listeners.has(event)) {
            this.listeners.delete(event);
        }
        return this;
    }


    get(event: string): Function | undefined {
        return this.listeners.get(event);
    }

    has(event: string): boolean {
        return this.listeners.has(event);
    }

}