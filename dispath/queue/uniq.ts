export class EventDipatcherQueueUniq {
    storage: (string | EventDipatcherQueueUniq)[];
    protected list: string[];
    name: string;
    async: boolean;
    constructor() {
        this.storage = [];
        this.list = [];
        this.async = false;
    }

    * [Symbol.iterator]() {
        for (let value of this.storage) {
            if (typeof value === 'string') {
                yield value;
            } else {
                yield value.name;
                for (let value2 of value) {
                    yield value2;
                }
            }
        }
    }
    /**
     * @todo добавить group,collection
     * @returns {string}
     */
    // * [Symbol.iterator]() {
    //     for (let value of this.storage) {
    //         if (typeof value === 'string') {
    //             yield [value, this.async];
    //         } else {
    //             yield [value.name, value.async,Array.from(value)];
    //             for (let value2 of value) {
    //                 yield value2;
    //             }
    //         }
    //     }
    // }

    start(): string {
        return this.list[0];

    }

    end(): string {
        return this.list[this.list.length - 1];
    }

    prev() {
    }

    next() {
    }

    listEvent(): string[] {
        return Array.from(this);
    }

    isGroup(): boolean {
        return (this.listGroup().length !== 0);
    }

    listGroup(): string[] {
        return this.storage
            .filter(value => {
                return (value instanceof EventDipatcherQueueUniq);
            })
            .map((value: EventDipatcherQueueUniq) => {
                return value.name;
            });
    }

    protected reList() {

        return this.storage
            .map(value => {
                return (value instanceof EventDipatcherQueueUniq) ? value.name : value;
            });
    }

    has(name: string): boolean {
        return (this.list.indexOf(name) !== -1);
    }

    unshift(names: (string | EventDipatcherQueueUniq)[] | (string | EventDipatcherQueueUniq)): this {
        if (names instanceof Array) {
            for (let i in names) {
                let name = names[i];
                if (name instanceof EventDipatcherQueueUniq) {
                    if (this.list.indexOf(name.name) !== -1 || names.indexOf(name.name, Number(i) + 1) !== -1) {
                        throw new Error(`already name:${name}`);
                    }
                } else {
                    if (this.list.indexOf(name) !== -1 || names.indexOf(name, Number(i) + 1) !== -1) {
                        throw new Error(`already name:${name}`);
                    }
                }

            }
            this.storage = [...names, ...this.storage];
        } else {
            if (names instanceof EventDipatcherQueueUniq) {
                if (this.list.indexOf(names.name) !== -1) {
                    throw new Error(`already name:${names.name}`);
                }
            } else {
                if (this.list.indexOf(names) !== -1) {
                    throw new Error(`already name:${names}`);
                }
            }

            this.storage.unshift(names);
        }
        this.reList();
        return this;
    }

    push(names: (string | EventDipatcherQueueUniq)[] | (string | EventDipatcherQueueUniq)): this {
        if (names instanceof Array) {
            for (let i in names) {
                let name = names[i];
                if (name instanceof EventDipatcherQueueUniq) {
                    if (this.list.indexOf(name.name) !== -1 || names.indexOf(name.name, Number(i) + 1) !== -1) {
                        throw new Error(`already name:${name}`);
                    }
                } else {
                    if (this.list.indexOf(name) !== -1 || names.indexOf(name, Number(i) + 1) !== -1) {
                        throw new Error(`already name:${name}`);
                    }
                }

                this.storage.push(name);
            }
        } else {
            if (names instanceof EventDipatcherQueueUniq) {
                if (this.list.indexOf(names.name) !== -1) {
                    throw new Error(`already name:${names.name}`);
                }
            } else {
                if (this.list.indexOf(names) !== -1) {
                    throw new Error(`already name:${names}`);
                }
            }
            this.storage.push(names);
        }
        this.reList();
        return this;
    }


    after(search: string, names: string[] | string): this {
        let index = this.list.indexOf(search);
        if (index === -1) {
            throw new Error(`not found name:${name}`);
        }
        index++;
        if (names instanceof Array) {
            for (let i in names) {
                let name = names[i];
                if (this.list.indexOf(name) !== -1 || names.indexOf(name, Number(i) + 1) !== -1) {
                    throw new Error(`already name:${name}`);
                }
            }
            this.storage = [...this.storage.slice(0, index), ...names, ...this.storage.slice(index)];
        } else {
            if (this.list.indexOf(names) !== -1) {
                throw new Error(`already name:${name}`);
            }
            this.storage = [...this.storage.slice(0, index), names, ...this.storage.slice(index)];
        }
        this.reList();
        return this;
    }

    before(search: string, names: string[] | string): this {
        let index = this.list.indexOf(search);
        if (index === -1) {
            throw new Error(`not found name:${name}`);
        }
        if (names instanceof Array) {
            for (let i in names) {
                let name = names[i];
                if (this.list.indexOf(name) !== -1 || names.indexOf(name, Number(i) + 1) !== -1) {
                    throw new Error(`already name:${name}`);
                }
            }
            this.storage = [...this.storage.slice(0, index), ...names, ...this.storage.slice(index)];
        } else {
            if (this.list.indexOf(names) !== -1) {
                throw new Error(`already name:${name}`);
            }
            this.storage = [...this.storage.slice(0, index), names, ...this.storage.slice(index)];
        }
        this.reList();
        return this;
    }
}