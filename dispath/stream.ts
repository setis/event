import {EventDispatcherEmitter} from "./emitter";
import uuidV4 = require('uuid/v4');
import {EventDispatcherListenerFace, EventDispatcherListenerDoubleDuplex} from "./listener/index";
import {EventDispatcherDoubleMode} from "./listener/double/index";
import {EventDispatcherListenerDoubleFullDuplex} from "./listener/double/fullduplex";
import {EventDispatcherListenerDoubleSimplex} from "./listener/double/simplex";
import {EventDispatcherListenerMany} from "./listener/many";
import {EventDispatcherListenerOnce} from "./listener/once";

export interface EventDispatcherStreamConfig {
    multi: boolean;
    mode: EventDispatcherDoubleMode;
}

export class EventDispatcherStream {
    protected source: EventDispatcherEmitter;
    readonly uuid: string;
    time: Date;
    event: string;
    data: Map<string, any>;
    params: Map<string, any>;
    readonly_listener: EventDispatcherListenerFace;
    listener: EventDispatcherListenerFace;
    lock: boolean;
    multi: boolean;
    mode: EventDispatcherDoubleMode;

    constructor(config: EventDispatcherStreamConfig, source: EventDispatcherEmitter, write?: EventDispatcherListenerFace) {
        this.uuid = uuidV4();
        this.time = new Date();
        this.params = new Map();
        this.data = new Map();
        let {multi = false, mode = EventDispatcherDoubleMode.none} = config;
        this.source = source;
        this.multi = multi;
        this.mode = mode;
        this.readonly_listener = source.listeners;
        this.lock = false;
        this.handler(write);

    }

    dispatch(params?: Object): Promise<EventDispatcherStream> {
        if (params) {
            for (let key in params) {
                this.params.set(key, params[key]);
            }
        }
        return EventDispatcherEmitter.executor(this.source.make(this.listener, this.mode), this);
    }

    protected handler(listener?: EventDispatcherListenerFace): this {
        if (!this.lock) {
            if (listener === undefined && (this.mode !== EventDispatcherDoubleMode.none )) {
                listener = (this.multi) ? new EventDispatcherListenerMany() : new EventDispatcherListenerOnce();
            }
            switch (this.mode) {
                case EventDispatcherDoubleMode.none:
                    ['on', 'off'].forEach((method) => {
                        this[method] = function () {
                            throw new Error(`lock method mode:none`);
                        };
                    });
                    this.listener = this.readonly_listener;
                    break;
                case EventDispatcherDoubleMode.simplex:
                    this.listener = new EventDispatcherListenerDoubleSimplex(this.readonly_listener, listener);
                    break;
                case EventDispatcherDoubleMode.duplex:
                    this.listener = new EventDispatcherListenerDoubleDuplex(this.readonly_listener, listener);
                    break;
                case EventDispatcherDoubleMode.fullduplex:
                    this.listener = new EventDispatcherListenerDoubleFullDuplex(this.readonly_listener, listener);
                    break;

            }
            this.lock = true;
        }
        return this;
    }

    on(event: string, listener: Function): this {
        this.handler();
        this.listener.on(event, listener);
        return this;
    }

    off(event: string, listener?: Function): this {
        this.handler();
        this.listener.off(event, listener);
        return this;
    }

    has(event: string): boolean {
        return this.listener.has(event);
    }

    get(event: string): Set<Function> {
        if (this.readonly_listener === this.listener) {
            return this.readonly_listener.get(event);
        }
        return this.listener.get(event);
    }


    list(): string[] {
        return this.listener.list();
    }
}