import {EventDispatcherEmitter} from "./emitter";
import {EventDispatcherStream} from "./stream";
import {EventDipatcherQueueUniq} from "./queue/uniq";

// let g = new EventDipatcherQueueUniq();
// g.name = 'event4';
// g.async = true;
// g.push(['event5', 'event6', 'event7','event1']);
//
// let q = new EventDipatcherQueueUniq();
// q.push(['event1', 'event2', 'event3']).push(g);
// console.log(q.storage);
// for (let name of q) {
//     console.log(name);
// }

process.on('unhandledRejection', function () {
    console.log(arguments);
});
process.on('rejectionHandled', function () {
    console.log(arguments);
});
let kernel = new EventDispatcherEmitter();
kernel.queue.push(['event1', 'event2', 'event3']);
kernel
    .on('event1', function () {
        return new Promise((resolve, reject) => {
            this.data.set('name', 46);
            resolve()
        });
    })
    .on('event2', function () {
        return new Promise((resolve, reject) => {
            this.data.set('name', 47);
            console.log(this.event);
            resolve()
        });
    })
    .on('event3', function () {
        return new Promise((resolve, reject) => {
            console.log(this.data.get('name'), this.event);
            resolve()
        });
    }).on('event4', function () {
        return new Promise((resolve, reject) => {
            console.log(this.data.get('name'), this.event);
            resolve()
        });
    }).on('event5', function () {
        return new Promise((resolve, reject) => {
            console.log(this.data.get('name'), this.event);
            resolve()
        });
    }).on('event6', function () {
        return new Promise((resolve, reject) => {
            console.log(this.data.get('name'), this.event);
            resolve()
        });
    }).on('event7', function () {
        return new Promise((resolve, reject) => {
            console.log(this.data.get('name'), this.event);
            resolve()
        });
    });
// let stream = new EventDispatcherStream();
kernel.stream().dispatch().then((self) => {
    console.log('done', self);
    // stream.data.forEach(console.log);
}, console.log);
kernel.dispatch().then((self) => {
    console.log('done', self);
    // stream.data.forEach(console.log);
}, console.log);

