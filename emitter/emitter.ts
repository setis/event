import {iEmitterListener} from "./listener/base";
import {EmitterStream, EmitterStreamConfig} from "./stream";
import {EmitterListenerOnce} from "./listener/once";
import {iEmitterDoubleMode} from "./listener/double/index";
/**
 * Created by alex on 11.07.17.
 */

export class Emitter implements iEmitterListener {
    listener: iEmitterListener;
    config: EmitterStreamConfig;

    constructor(config?: EmitterStreamConfig, listener?: iEmitterListener) {
        this.config = (config === undefined) ? {multi: true, mode: iEmitterDoubleMode.simplex} : config;
        if (listener === undefined) {
            this.listener = new EmitterListenerOnce();
            return;
        }
        this.listener = listener;

    }

    on(event: string, listener: Function): this {
        this.listener.on(event, listener);
        return this;
    }

    off(event: string, listener?: Function): this {
        this.listener.off(event, listener);
        return this;
    }

    once(event: string, listener: Function): this {
        this.listener.once(event, listener);
        return this;
    }

    hasListener(event: string): boolean {
        return this.listener.hasListener(event);
    }

    getListener(event: string): Function | Set<Function>{
        return this.listener.getListener(event);
    }

    events(): string[] {
        return this.listener.events();
    }

    emit(event: string, ...args): EmitterStream {

        let stream = new EmitterStream(this.config, this.listener);
        this.listener.emit.apply(this.listener, [event, stream, ...args]);
        return stream;
    }


    stream(config?: EmitterStreamConfig): EmitterStream {
        return new EmitterStream((config === undefined) ? this.config : config, this.listener);
    }

}