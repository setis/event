/**
 * Created by alex on 10.03.17.
 */

import {EmitterStream} from "../stream";

export interface iEmitterListener {

    on(event: string, listener: Function): this;

    off(event: string, listener?: Function): this;

    once(event: string, listener: Function): this;

    hasListener(event: string): boolean;

    getListener(event: string);

    events(): string[];

    emit(event: string, context: EmitterStream, ...args);

    emit(event: string, ...args): EmitterStream | void;

    emit(event: string, ...args): void;
}
