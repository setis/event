import {EmitterStream} from "../../stream";
import {iEmitterListener} from "../base";
import {EmitterListenerOnce} from "../once";
import {uniq} from "lodash";
/**
 * Created by alex on 13.07.17.
 */

export abstract class EmitterListenerDoubleBase{
    read: iEmitterListener;
    write: iEmitterListener;

    constructor(read?: iEmitterListener, write?: iEmitterListener) {
        if (read !== undefined) {
            this.read = read;
        }
        if (write !== undefined) {
            this.write = write;
        } else {
            this.write = new EmitterListenerOnce();
        }

    }

    setReadListener(listener: iEmitterListener) {
        this.read = listener;
    }

    setWriteListener(listener: iEmitterListener) {
        this.write = listener;
    }

    events(): string[] {
        let events: string[] = this.write.events();
        events.concat(this.read.events());
        return uniq(events);
    }

    on(event: string, listener: Function): this {
        this.write.on(event, listener);
        return this;
    }

    off(event: string, listener?: Function): this {
        this.write.off(event, listener);
        return this;
    }

    once(event: string, listener: Function): this {
        this.write.once(event, listener);
        return this;
    }

    abstract getListener(event: string);

    hasListener(event: string): boolean {
        return (this.write.hasListener(event) || this.read.hasListener(event));
    }

    abstract emit(event: string, context: EmitterStream, ...args);
}