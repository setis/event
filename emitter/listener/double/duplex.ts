/**
 * Created by alex on 13.07.17.
 */


import {iEmitterListener} from "../base";
import {uniq} from "lodash";
import {EmitterListenerOnce} from "../once";
import {EmitterStream} from "../../stream";
import {EmitterListenerDoubleBase} from "./base";
import {listeners} from "cluster";
/**
 * Created by alex on 14.03.17.
 */

export class EmitterListenerDoubleDuplex extends EmitterListenerDoubleBase implements iEmitterListener {

    getListener(event: string): Set<Function> {
        let funcs = new Set();
        if (this.read.hasListener(event)) {
            let funcRead = this.read.getListener(event);
            if (funcRead instanceof Set) {
                funcRead.forEach((listener) => {
                    funcs.add(listener);
                })
                // }else if(typeof funcRead === 'function'){
            } else {
                funcs.add(funcRead);
            }
        }
        if (this.write.hasListener(event)) {
            let funcWrite = this.write.getListener(event);
            if (funcWrite instanceof Set) {
                funcWrite.forEach((listener) => {
                    funcs.add(listener);
                })
                // }else if(typeof funcRead === 'function'){
            } else {
                funcs.add(funcWrite);
            }
        }

        return funcs;
    }

    emit(event: string, context: EmitterStream, ...args) {
        if (!this.hasListener(event)) {
            return;
        }
        this.read.emit(event, context, ...args);
        this.write.emit(event, context, ...args);
    }


}
