/**
 * Created by alex on 13.07.17.
 */


import {iEmitterListener} from "../base";
import {uniq} from "lodash";
import {EmitterListenerOnce} from "../once";
import {EmitterStream} from "../../stream";
import {EmitterListenerDoubleBase} from "./base";
/**
 * Created by alex on 14.03.17.
 */

export class EmitterListenerDoubleFullDuplex extends EmitterListenerDoubleBase implements iEmitterListener {

    getListener(event: string): Set<Function> {
        throw new Error();
    }

    emit(event: string, context: EmitterStream, ...args) {
        throw new Error();
    }


}
