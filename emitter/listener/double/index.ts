/**
 * Created by alex on 13.07.17.
 */

export interface iEmitterDouble {

}
export * from './base';
export * from './duplex';
export * from './simplex';
export * from './fullduplex';
export enum iEmitterDoubleMode{
    simplex,
    duplex,
    fullduplex
}
export type EmitterDoubleMode = 'simplex' | 'duplex' | 'fullduplex';