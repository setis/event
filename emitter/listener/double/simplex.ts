/**
 * Created by alex on 13.07.17.
 */


import {iEmitterListener} from "../base";
import {uniq} from "lodash";
import {EmitterListenerOnce} from "../once";
import {EmitterStream} from "../../stream";
import {EmitterListenerDoubleBase} from "./base";
/**
 * Created by alex on 14.03.17.
 */

export class EmitterListenerDoubleSimplex extends EmitterListenerDoubleBase implements iEmitterListener {

    getListener(event: string): Function | Set<Function> | void {
        if (this.write.hasListener(event)) {
            return this.write.getListener(event);
        }
        if (this.read.hasListener(event) !== false) {
            return this.read.getListener(event);
        }
    }

    emit(event: string, context: EmitterStream, ...args) {
        if (!this.hasListener(event)) {
            return;
        }
        let listener = this.getListener(event);
        if (typeof listener === 'function') {
            listener.apply(context, args);
        } else if (listener instanceof Set) {
            listener.forEach((func) => {
                func.apply(context, args)
            })
        }
    }


}
