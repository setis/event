import {iEmitterListener} from "./base";
import {EmitterStream} from "../stream";
/**
 * Created by alex on 11.07.17.
 */

export class EmitterListenerMany implements iEmitterListener {
    listeners: Map<string, Set<Function>>;

    constructor() {
        this.listeners = new Map();
    }

    on(event: string, listener: Function): this {
        if (this.listeners.has(event)) {
            let l = this.listeners.get(event);
            if (l === undefined) {
                this.listeners.set(event, new Set([listener]));
            }
            else if (l instanceof Set) {
                l.add(listener);
            } else {
                this.listeners.set(event, new Set([l, listener]));
            }
        } else {
            this.listeners.set(event, new Set([listener]));
        }
        return this;
    }

    off(event: string, listener?: Function): this {
        if (this.listeners.has(event)) {
            if (listener === undefined) {
                this.listeners.delete(event);
            } else {
                let listeners = this.listeners.get(event);
                if (listeners instanceof Set) {
                    listeners.delete(listener);
                    if (listeners.size === 0) {
                        this.listeners.delete(event);
                    }
                }
            }
        }
        return this;
    }

    once(event: string, listener: Function): this {
        throw new Error(`not method once`);
    }

    hasListener(event: string): boolean {
        return this.listeners.has(event);
    }

    getListener(event: string): Set<Function> | void {
        return this.listeners.get(event);
    }

    events(): string[] {
        return Array.from(this.listeners.keys());
    }

    emit(event: string, context: EmitterStream, ...args) {
        if (!this.listeners.has(event)) {
            return;
        }
        let listeners = this.listeners.get(event);
        if (listeners === undefined) {
            return;
        }
        listeners.forEach((listener) => {
            listener.apply(context, args);
        });
    }

}