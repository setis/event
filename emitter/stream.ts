/**
 * Created by alex on 11.07.17.
 */
/**
 * Created by alex on 11.03.17.
 */
import uuidV4 = require('uuid/v4');
import {iEmitterListener} from "./listener/base";
import {EmitterDoubleMode, iEmitterDoubleMode} from "./listener/double";
import {EmitterListenerOnce} from "./listener/once";
import {EmitterListenerMany} from "./listener/many";
import {EmitterListenerDoubleSimplex} from "./listener/double/simplex";
import {EmitterListenerDoubleDuplex} from "./listener/double/duplex";
import {EmitterListenerDoubleFullDuplex} from "./listener/double/fullduplex";

export interface EmitterStreamConfig {
    multi: boolean,
    mode: iEmitterDoubleMode | EmitterDoubleMode;
}

export class EmitterStream implements iEmitterListener {

    uuid: string;
    time: Date;
    event: string;
    readonly_listener: iEmitterListener;
    listener: iEmitterListener;
    data: Map<string, any> | any;
    params: Map<string, any> | any;
    lock: boolean;
    multi: boolean;
    mode: iEmitterDoubleMode | EmitterDoubleMode;

    constructor(config: EmitterStreamConfig, read: iEmitterListener, write?: iEmitterListener) {
        let {multi = false, mode = iEmitterDoubleMode.simplex} = config;
        this.uuid = uuidV4();
        this.time = new Date();
        this.lock = false;
        this.readonly_listener = read;
        this.multi = multi;
        this.mode = mode;
        this.params = new Map();
        this.data = new Map();
        this.handlerListener(write);
    }

    handlerListener(listener?: iEmitterListener): this {
        if (!this.lock) {
            if (listener === undefined) {
                listener = (this.multi) ? new EmitterListenerMany() : new EmitterListenerOnce();
            }
            switch (this.mode) {
                case 'simplex':
                case iEmitterDoubleMode.simplex:
                    this.listener = new EmitterListenerDoubleSimplex(this.readonly_listener, listener);
                    break;
                case 'duplex':
                case iEmitterDoubleMode.duplex:
                    this.listener = new EmitterListenerDoubleDuplex(this.readonly_listener, listener);
                    break;
                case 'fullduplex':
                case iEmitterDoubleMode.fullduplex:
                    this.listener = new EmitterListenerDoubleFullDuplex(this.readonly_listener, listener);
                    break;

            }
            this.lock = true;
        }
        return this;
    }

    on(event: string, listener: Function): this {
        this.handlerListener();
        this.listener.on(event, listener);
        return this;
    }

    off(event: string, listener?: Function): this {
        this.handlerListener();
        this.listener.off(event, listener);
        return this;
    }

    once(event: string, listener: Function): this {
        throw new Error("Method not implemented.");
    }

    hasListener(event: string): boolean {
        return this.listener.hasListener(event);
    }

    getListener(event: string): Set<Function> {
        if (this.readonly_listener === this.listener) {
            return this.readonly_listener.getListener(event);
        }
        return this.listener.getListener(event);
    }


    events(): string[] {
        return this.listener.events();
    }

    emit(event: string, ...args): void {
        if (args.length === 0 && this.params.has(event)) {
            let params = this.params.get(event);
            if (params instanceof Array) {
                this.listener.emit(event, this, ...params);
            } else {
                this.listener.emit(event, this, params);
            }

        } else {
            this.listener.emit(event, this, ...args);
        }

    }
}