/**
 * Created by alex on 18.06.17.
 */
import {Dispatcher} from "../dispatcher";
import * as assert from "assert";
import * as microtime from 'microtime';
import {expect} from 'chai';
describe('dispatcher', function () {
    it('проверка очереди если одного всписке собития не существует', function (done) {
        let event = new Dispatcher();
        event.listeners
            .on('begin', function (callback, i) {
                this.data.start = process.hrtime();
                callback(null, i);
            })
            .on('end', function (callback) {
                done();
                callback(null);
            });
        event.queue().add([
            'begin',
            'asd',
            'end',
            'sdd'
        ]);
        event.dispatch();
    });
    it('ввод данных и получение результата', function (done) {
        let event = new Dispatcher();
        event.listeners
            .on('begin', function (callback, i) {
                this.data.i = 2;
                callback(null, i);
            })
            .on('sum', function (callback) {
                this.data.i += 2;
                callback(null);
            })
            .on('assert', function (callback) {
                assert.equal(this.data.i, 4);
                callback(null);
            })
            .on('end', function (callback) {
                done();
                callback(null);
            });
        event.queue().add([
            'begin',
            'sum',
            'assert',
            'end',
        ]);
        event.dispatch();
    });
    it('проверка асинхроная группа событий', function (done) {
        let event = new Dispatcher();
        event.listeners
            .on('begin', function (callback, i) {
                this.data.i = 2;
                callback(null, i);
            })
            .on('sum', function (callback) {
                this.data.i += 2;
                callback(null);
            })
            .on('assert', function (callback) {
                assert.equal(this.data.i, 4);
                callback(null);
            })
            .on('event1', function (callback) {

                setTimeout(() => {
                    this.data.event1 = microtime.now();
                    callback(null);
                }, 15);

            })
            .on('event2', function (callback) {
                setTimeout(() => {
                    this.data.event2 = microtime.now();
                    callback(null);
                }, 1);
            })
            .on('end', function (callback) {
                assert.equal((this.data.event1 - this.data.event2) > 0, true);
                done();
                callback(null);
            });
        event.collection().create('event', ['event1', 'event2', 'assert']);
        event.queue().add([
            'begin',
            'sum',
            'event',
            'assert',
            'end',
            'asd',
        ]);
        event.dispatch(2);
    });
    it('проверка группу событий', function (done) {
        let event = new Dispatcher();
        event.listeners
            .on('begin', function (callback, i) {
                this.data.i = 2;
                callback(null, i);
            })
            .on('sum', function (callback) {
                this.data.i += 2;
                callback(null);
            })
            .on('assert', function (callback) {
                assert.equal(this.data.i, 4);
                callback(null);
            })
            .on('event1', function (callback) {

                setTimeout(() => {
                    this.data.event1 = microtime.now();
                    callback(null);
                }, 15);

            })
            .on('event2', function (callback) {
                setTimeout(() => {
                    this.data.event2 = microtime.now();
                    callback(null);
                }, 1);
            })
            .on('end', function (callback) {
                assert.equal((this.data.event2 - this.data.event1) > 0, true);
                done();
                callback(null);
            });
        event.group().create('event', ['event1', 'event2', 'assert']);
        event.queue().add([
            'begin',
            'sum',
            'event',
            'assert',
            'end',
            'asd',
        ]);
        event.dispatch(2);
    });
    it('проверка списка очереди', function (done) {
        let event = new Dispatcher();
        event.listeners
            .on('begin', function (callback, i) {
                this.data.i = 2;
                callback(null, i);
            })
            .on('sum', function (callback) {
                this.data.i += 2;
                callback(null);
            })
            .on('group:assert', function (callback) {
                assert.equal(this.data.i, 4);
                assert.equal((this.data.group_event2 - this.data.group_event1) > 0, true);
                callback(null);
            })
            .on('collection:assert', function (callback) {
                assert.equal(this.data.i, 4);
                assert.equal((this.data.collection_event1 - this.data.collection_event2) > 0, true);
                callback(null);
            })
            .on('group:event1', function (callback) {
                setTimeout(() => {
                    this.data.group_event1 = microtime.now();
                    callback(null);
                }, 15);

            })
            .on('group:event2', function (callback) {
                setTimeout(() => {
                    this.data.group_event2 = microtime.now();
                    callback(null);
                }, 1);
            })
            .on('collection:event1', function (callback) {
                setTimeout(() => {
                    this.data.collection_event1 = microtime.now();
                    callback(null);
                }, 15);

            })
            .on('collection:event2', function (callback) {
                setTimeout(() => {
                    this.data.collection_event2 = microtime.now();
                    callback(null);
                }, 1);
            })
            .on('end', function (callback) {
                expect(event.queue().list).to.include.members(queue);
                done();
                callback(null);
            });
        event.group().create('group', ['group:event1', 'group:event2', 'group:assert']);
        event.collection().create('collection', ['collection:event1', 'collection:event2']);
        let queue = [
            'begin',
            'sum',
            'collection',
            'collection:assert',
            'group',
            'end',
        ];
        event.queue().add(queue);
        event.dispatch(2);

    });
});