/**
 * Created by alex on 18.06.17.
 */
import * as assert from "assert";
import * as microtime from 'microtime';
import {expect} from 'chai';
import {Emitter} from "../emitter/emitter";
import {EmitterStream} from "../emitter/stream";
import {EmitterListenerMany} from "../emitter/listener/many";

describe('emitter', function () {

    it('проверка emit args', function (done) {
        let event = new Emitter();
        event
            .on('sum', function (a, b) {
                this.emit('data', a + b);
                setTimeout(() => {
                    this.emit('done');
                }, 15);
            })
            .on('data', function (sum) {
                assert.equal(sum, 5);
            })
            .on('done', function () {
                done();
            });
        event.stream().emit('sum', 2, 3);

    });
    it('проверка emit params', function (done) {
        let event = new Emitter();
        event
            .on('sum', function (a, b) {
                this.emit('data', a + b);
                setTimeout(() => {
                    this.emit('done');
                }, 15);
            })
            .on('data', function (sum) {
                assert.equal(sum, 5);
            })
            .on('done', function () {
                done();
            });
        let stream = event.stream();
        stream.params.set('sum',[2,3]);
        stream.emit('sum');

    });
    it('проверка данных steam', function (done) {
        let event = new Emitter();
        event.on('welcome', function (name) {
            assert.equal(name, 'Alex');
            assert.equal(this instanceof EmitterStream, true);
            done();
        });
        event.stream().emit('welcome', 'Alex');

    });
    it('попытка установить еще раз одно и тоже событие', function () {
        let event = new Emitter();
        event.on('welcome', function (name) {
        });
        assert.throws(function () {
            event.on('welcome', function (name) {
            });
        }, Error, 'listener.storage.has_one event:welcome already exists listener');
        event
            .off('welcome')
            .on('welcome', function (name) {
            });
    });
    it(`new Emitter({multi: false, mode: 'simplex'})`, function (done) {
        let event = new Emitter({multi: false, mode: 'simplex'});
        event.on('welcome', function (name) {
            done(new Error())
        });
        let funcs = event.getListener('welcome');
        assert.equal(funcs instanceof Set, false);
        assert.equal(typeof funcs, 'function');
        let stream = event
            .stream()
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                done();
            });
        stream.emit('welcome', 'Alex');
        assert.equal(stream.getListener('welcome').size, 3);
    });
    it(`new Emitter({multi: true, mode: 'simplex'})`, function (done) {
        let event = new Emitter({multi: true, mode: 'simplex'});
        event.on('welcome', function (name) {
            done(new Error())
        });
        let funcs = event.getListener('welcome');
        assert.equal(funcs instanceof Set, false);
        assert.equal(typeof funcs, 'function');
        let stream = event.stream();
        let i = 0;
        stream.data = {};
        stream.data.i = 0;
        stream
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                i++;
                this.data.i++;
            })
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                i++;
                this.data.i++;
            })
            .emit('welcome', 'Alex');
        assert.equal(stream.getListener('welcome').size, 2);
        setTimeout(() => {
            assert.equal(i, 2);
            assert.equal(stream.data.i, 2);
            done();
        }, 5);
    });
    it(`new Emitter({multi: true, mode: 'duplex'})`, function (done) {
        let event = new Emitter({multi: true, mode: 'duplex'});
        let i = 0;
        event
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                i++;
                this.data.i++;
            });
        let funcs = event.getListener('welcome');
        assert.equal(funcs instanceof Set, false);
        assert.equal(typeof funcs, 'function');
        let stream = event.stream();

        stream.data = {};
        stream.data.i = 0;
        stream
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                i++;
                this.data.i++;
            })
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                i++;
                this.data.i++;
            })
            .emit('welcome', 'Alex');
        assert.equal(stream.getListener('welcome').size, 3);
        setTimeout(() => {
            assert.equal(i, 3);
            assert.equal(stream.data.i, 3);
            done();
        }, 5);
    });
    it(`new Emitter({multi:true,mode:'duplex'},new EmitterListenerMany())`, function (done) {
        let event = new Emitter({multi: true, mode: 'duplex'}, new EmitterListenerMany());
        let i = 0;
        event
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                i++;
                this.data.i++;
            })
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                i++;
                this.data.i++;
            });
        let funcs = event.getListener('welcome');
        assert.equal(funcs instanceof Set, true);
        if (funcs instanceof Set) {
            assert.equal(funcs.size, 2);
        }
        let stream = event.stream();
        stream.data = {};
        stream.data.i = 0;
        stream
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                i++;
                this.data.i++;
            })
            .on('welcome', function (name) {
                assert.equal(name, 'Alex');
                assert.equal(this instanceof EmitterStream, true);
                i++;
                this.data.i++;
            })
            .emit('welcome', 'Alex');
        assert.equal(stream.getListener('welcome').size, 4);
        setTimeout(() => {
            assert.equal(i, 4);
            assert.equal(stream.data.i, 4);
            done();
        }, 5);
    });

});