/**
 * Created by alex on 18.06.17.
 */
import {EventDispatcherEmitter} from "../dispath";
import * as assert from "assert";
import * as microtime from 'microtime';
import {expect} from 'chai';
import {EventDispatcherStream} from "../dispath/stream";

describe('dispatcher', function () {
    it('проверка очереди если одного всписке собития не существует', function (done) {
        let kernel = new EventDispatcherEmitter();
        kernel.queue.push(['event1', 'event2', 'event3']);
        kernel
            .on('event1', function () {
                return new Promise((resolve, reject) => {
                    assert.equal(this.data.has('name'),false);
                    this.data.set('name', 46);
                    resolve()
                });
            })
            .on('event2', function () {
                return new Promise((resolve, reject) => {
                    assert.equal(this.data.get('name'),46);
                    this.data.set('name', 47);
                    resolve()
                });
            })
            .on('event3', function () {
                return new Promise((resolve, reject) => {
                    assert.equal(this.data.get('name'),47);
                    this.data.set('name', 48);
                    resolve()
                });
            });
        kernel.stream().dispatch().then((self) => {
            assert.equal(self instanceof EventDispatcherStream, true);
            assert.equal(self.data.get('name'), 48);
            done();
        });
    });

});